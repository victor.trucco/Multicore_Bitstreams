# Sega Master System

Formato do SD: FAT32 sem suporte a nomes logos de arquivo.

Soft Reset : Botão 3

Pause : Botão 4

Hard Reset: Botão 3 + Botão 4 volta à tela do Loader

Scanlines opcionais: Botão 1 + Botão 2 ativam/desativam scanlines

Arquivos JIC e SOF, de acordo com a versão, VGA ou HDMI.


##### Bugs conhecidos
 
- "Glitches" nas imagens, de tempos em tempos.
- Sem som no HDMI
- Loader não foi bem testado, pode conter bugs

##### Change log

- 001 : 04/12/2016 - versão inicial