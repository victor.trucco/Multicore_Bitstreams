# Nintendo Entertainment System

Formato do SD: FAT16 ou FAT32 com nomes longos de arquivos, compatível com arquivos ".NES"

Botão 1: Start player 1

Botão 2: Select player 1

Botão 3: Start player 2

Botão 4: Select player 2


Reset: Botão 3 + Botão 4 volta à tela de menu do Loader


Scanlines: pode ser selecionadas diretamente no menu do Loader.


A memória foi dividida em 256Kb para a PRG ROM e 256Kb para a CHR ROM. 
Não conheço nenhum que ultrapasse esses limites, mas teoricamente podem existir jogos que usem mais.



Arquivos JIC e SOF, de acordo com a versão.




##### Bugs conhecidos

- Algumas vezes ocorre erro durante o carregamento da ROM. Nesse momento os leds do Multicore se iluminam, 
porém nãom podem ser visto diretamente por estarem na parte de dentro da placa, 
mas é bem fácil de perceber porque eles iluminam os quatro botões do MC. 
Basta repetir a operação e o jogo é carregado.





##### Change log

- 001 : 17/12/2016 - versão inicial
- 002 : 20/02/2018 - Suporte ao Multicore 2