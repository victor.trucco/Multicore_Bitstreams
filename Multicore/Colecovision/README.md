# Colecovision

Formato do SD: FAT16

Descompactar o arquivo "SD Card.rar" na raiz do cartão SD, após a formatação. As roms devem ir na pasta "Coleco" (elas não estão inclusas no arquivo compactado).

Necessita de teclado conectado para simular as teclas numéricas dos controles do Colecovision.

Arquivos JIC e SOF, de acordo com a versão, VGA ou HDMI.


##### Change log

- 003 : 20/02/2018 - Suporte ao Multicore 2
- 002 : 25/03/2017 - Melhorias internas
- 001 : 24/11/2016 - versão inicial

