# Apple IIe
- [Código-Fonte no Gitlab](https://gitlab.com/victor.trucco/Multicore/-/tree/master/Computers/Apple%20IIe)

- VGA
- 80 colunas (slot 0)
- Mocking Board (slot 4)
- Saturn 128k (slot 5)
- Interface DiskII (slot 6)

Hard Reset: Botão 3
Soft Reset: Botão 4

#### Uso

Colocar o arquivo "Apple IIe.MCP" na raiz de um cartão SD para o boot. As imagens de disco são ".NIB", convertidas com o programa DSK2NIB. 
As imagens convertidas podem ser organizadas em pastas, no cartão SD.

As imagens .NIB podem ser carregadas com a tecla F12 e são selecionadas com as setas e ENTER. Após, executar um "Hard Reset" pressionando o botão 3 para que a DiskII execute o boot do disco.

##### Change log

  004 : 27/10/2022 - Acerto para o firmware STM 2.0
- 003 : 03/12/2020 - Versão convertida para o MC2+
- 002 : 03/08/2020 - Correção para jogos de multiplos discos
- 001 : 10/10/2019 - versão inicial