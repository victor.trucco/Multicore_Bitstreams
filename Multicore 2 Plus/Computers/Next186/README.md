# Next186 - PC-XT

Requer cartão SDHC classe 10. Não funciona com cartões SD tradicionais.

Requer uma imagem gravada e também a BIOS nos últimos blocos do cartão. O procedimento para gravação pode ser visto em https://multicore-2.fandom.com/pt-br/wiki/Next186

##### Change log

- 001 : 10/11/2020 - versão inicial