# Core Amiga

## Instruções para instalação

- Descompacte o arquivo Amiga.zip
- Copie todos os arquivos descompactados para a raiz do cartão SD, todos precisam estar na raiz, exceto o Amiga.MCP que pode estar em qualquer pasta. 
- As imagens de disco (*.ADF) podem ficar em qualquer pasta do cartão SD

## Joystick

A entrada de joystick 1 fica na porta da direita, para parecer o máximo com o original :)
