# ZX81

[Código-Fonte no Gitlab](https://gitlab.com/victor.trucco/Multicore/-/tree/master/Computers/ZX8X%20(MC2P))

- VGA
- suporte a interface de som ZON-X
- Redefinição de caracteres CHR128
- Carregamento via conector auxiliar
- Carregamento de arquivos .P direto pelo menu
- Vsync ligado ao audio para simular "interferência" gerada por alguns programas

Hard Reset: Botão 1

#### Uso

Descompacte o arquivo .zip em qualquer pasta do seu cartão SD. Os jogos .P também podem estar em pastas separadas. 

Use F12 para abrir o menu de opções. Para carregar um programa, selecione a opção "LOAD *.P" no menu, escolha o arquivo e em seguida digite LOAD "" e pressione ENTER.


##### Change log

  009 : 10/06/2022 - Suporte a entrada de audio (tape) externo. 
  008 : 12/08/2021 - Adicionado saida MIC para gravação. Adicionado VSync como audio, para alguns software com simulação de som. 
- 007 : 11/01/2021 - Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
- 006 : 30/12/2020 - Limpeza geral nos clocks (agora com "clock_enable"). Usando agora a sintetização JT49 (by Jotego) para o AY.
- 005 : 16/08/2020 - Suporte a Chroma 81 e redefinidor de caracteres Quicksilva.
- 004 : 02/12/2019 - Menus de configuração adicionados
- 003 : 13/10/2019 - nova versão, port do ZX8X
- 002 : 09/07/2018 - versão inicial Multicore 2
- 001 : 25/01/2016 - versão inicial