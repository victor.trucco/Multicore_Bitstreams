Monte o cartão sd com os arquivos do site oficial (use o "board backers edition").
https://www.specnext.com/latestdistro/

Descompacte e coloque o arquivo do core "Next.mcp" na raiz do cartão SD. Renomeie para "core.mcp" se você quiser o boot de maneira automática, quando ligar o Multicore 2 plus.

CHANGELOG

003 - 04/07/2022 - Acerto para "unsupported hardware ID" no menu
002 - 13/08/2021 - Adicionado saida de som MIC. Suporte ao barramento de GPIO
001 - 01/02/2021 - versão inicial