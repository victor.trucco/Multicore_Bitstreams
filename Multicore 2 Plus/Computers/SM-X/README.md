SM-X Core
=========

Conversão do SM-X (MSX 2+) para o Multicore 2 plus.

Baseado no código de Luca "KdL" Chiodi. Arquivos originais em
https://gnogni.altervista.org/

## Como funciona

Existem duas maneiras de preparar o cartão SD para uso:

#### Usando a imagem pronta

Descompacte o arquivo ./sdimg/SD_SM-X.ZIP para obter o arquivo SD_SM-X.img. Opcionalmente você pode copiar apenas este arquivo para o seu cartão SD, junto com outros cores.

Para utilizar um cartão "aberto", exclusivo para o SM-X, use o Etcher ou Win32DiskImager para gravar essa imagem em um cartão SD (mínimo 1 Gb).

Após a gravação, copiar o arquivo "SM-X.MCP" para a raiz do cartão. Renomeie-o para "core.MCP" se você deseja que o boot seja automático.

É necessário a criação de um arquivo "SM-X.ini" (ou "core.ini", a depender do nome do seu MCP) com a linha "DIS_SD", para utilização do cartão "aberto".

#### Gerando a partir do sdcreate

Existem 2 scripts sdcreate.zip: um para as versões anteriores à bios KDL 3.9 (sdcreate-ocm38.zip) e outro para as últimas versões.
Na dúvida, use o sdcreate.zip

No diretório ./sdcreate existem 2 scripts, um para Windows e outro para Linux, para
gerar um cartão SD utilizando toda a capacidade do mesmo (até o tamanho máximo de
uma partição FAT16: 4GB)

Mais informações no README dentro do sdcreate.zip

# Joystick

- Mega Drive: B e A botões 1 e 2

- Master: botões 1 e 2

- Mega Drive 6 botões: B e A botões 1 e 2, MODE+START chama o OSD

# Outros

Botão 1 - Reset

Segurar F1 na inicialização: entra na configuração do WiFi, caso esteja usando a Bios 3.9 ou superior.

##### Change log

- 010 : 19/02/2023 - Suporte de acesso à configuração do WiFi pela Bios

- 009 : 23/11/2021 - Suporte direto a gravação do arquivo .INI, usando o cartão "aberto".   

- 008 : 11/10/2021 - Suporte ao Expansion #02 (Wifi e Slots externos)   

- 007 : 15/01/2020 - Atualizado 'sdcreate' para gerar cartões com SofaRun 7.0, DOSTOOLS completo, feita uma limpeza nos diretórios de sistema e corrigido erro na geração do diretório MM no script Windows

- 006 : 09/01/2020 - Suporte a mouse na porta PS/2 frontal

- 005 : 08/01/2020 - Seleção de layout de teclado pelo menu OSD

- 004 : 26/12/2020 - Turbo na tecla F8 (no F12 apenas o menu OSD)

- 003 : 25/12/2020 - SM-X sobe sem teclado, em joystick de 6 botões MODE + START chama o OSD pelo joystick

- 002 : 28/11/2020 - suporte a joysticks

- 001 : 13/10/2020 - versão inicial
