# MSX1fpga

Versao 1.3 by Fabio Belavenuto

Descompactar o arquivo "SD_Card.img" do zip e utilize um programa para gravar a imagem no cartão SD. (Exemplo Win32diskImager ou Etcher)

Copiar o arquivo "MSX1fpga.MCP" na raiz do cartão SD.

A máquina sintetizada é um MSX 1.0 de 128Kb de memória com som FM e SCC.


##### Change log

- 002 : 20/02/2021 - adicionado suporte de video a 15khz
- 001 : 04/02/2021 - versão inicial