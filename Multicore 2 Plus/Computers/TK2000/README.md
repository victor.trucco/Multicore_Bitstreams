# TK2000

[Código-Fonte no Gitlab](https://gitlab.com/victor.trucco/Multicore/-/tree/master/Computers/TK2000)

- VGA
- HDMI
- Drive DiskII
- Carregamento de arquivos a partir de audio
- Som do movimento da cabeça do drive (experimental)

Soft Reset: Botão 3
Hard Reset: Botão 3 + botão 4

#### Uso

Coloque o arqui .MCP no raiz do seu cartão SD. Os arquivos .NIB podem estar em pastas, no mesmo SD. 

Os arquivos .NIB são conversões dos DSK originais do TK2000. O conversor se encontra na pasta /Utils.

Use F12 para abrir o menu de opções. Para carregar um programa, selecione a opção "LOAD *.NIB" no menu e aguarde o boot do TK2000.

Verifique a pasta "/Utils" para maiores informações sobre a imagem dos discos para o cartão SD


##### Change log

- 004 : 10/04/2021 - Melhor paleta de cores. Corrigido timings de clock. substituida a CPU antiga pelo modulo T65.
- 003 : 09/12/2019 - HDMI e suporte a discos em FAT32
- 002 : 09/07/2018 - versão inicial Multicore 2
- 001 : 16/03/2017 - versão inicial