- [English](README.md)

# Multicore 2 Plus

Neste manual irá encontrar instruções básicas de operação do Multicore 2 Plus.

![Multicore 2 Plus](MC2P-main.jpg)

## Hardware

- Cyclone IV 55K LEs
- STM32
- SRAM 2048kb
- SDRAM 256Mb
- VGA
- HDMI (opcional)
- Saída de áudio estéreo
- Interface de cassete (EAR e MIC)
- 2 portas PS/2
- 2 portas Joystick
- 4 botões
- slot micro SD
- GPIO 60 pinos

### Frente

![MC2P Front](MC2P-front.jpg)

### Traseira

![MC2P Back](MC2P-back.jpg)

### Joysticks

As portas de joystick DB9 estão configuradas para aceitar joysticks Mega Drive/Sega Genesis de 6 botões.

Também é possível conectar joysticks MD/SG de 3 botões e Atari 1 botão.

*ATENÇÃO*: Não conecte joysticks padrão MSX pois poderá danificar o Multicore 2 Plus !!!

### Interface de cassete

O conector preto, entre o conector verde e o de teclado PS/2 , é a interface de cassete do Multicore 2 Plus.

![Cassette Conector Plug](cassette_interface.png)

### Funções

#### Botões

No corpo do Multicore 2 Plus irá encontrar 4 botões coloridos que podem variar dependendo do Core escolhido. Mas, normalmente, a função dos mesmos é a seguinte:

- Amarelo\*: Reset Geral
- Vermelho: Insere crédito (Arcade)
- Verde: Start 1P (Arcade)
- Azul: Start 2P (Arcade)

\* Na tela de carregamento dos Cores, ao pressionar o botão amarelo irá iniciar o modo de testes

#### Joystick

Na versão original do joystick Megadrive de 6 botões existe um botão chamado MODE, no qual é possível acionar algumas funções.

As funções de joystick são definidas de acordo com o Core carregado mas, normalmente, poderá acionar as seguintes funções na maioria dos mesmos:

- MODE + START: Abre OSD
- MODE + B: Insere crédito (Arcade)
- MODE + X: Start 1P (Arcade)
- MODE + Y: Start 2P (Arcade)

Consulte a documentação do Core para mais informações sobre as funções MODE.

#### Teclado

- F12: Abre o menu de opções do Core
- ScrollLock: Inverte o estado de saída (15khz)

### Configuração do cartão SD

#### Carregamento automático

Para carregar um Core automaticamente, renomeie-o para `core.mcp` na raiz do cartão SD
