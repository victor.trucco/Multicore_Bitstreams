- [Português BR](README_ptbr.md)

# Multicore 2 Plus

In this manual you will find basic operating instructions for Multicore 2 Plus.

![Multicore 2 Plus](MC2P-main.jpg)

## Hardware

- Cyclone IV 55K LEs
- STM32
- SRAM 2048kb
- SDRAM 256Mb
- VGA
- HDMI (optional)
- Stereo audio output
- Cassette interface (EAR e MIC)
- 2 PS/2 ports
- 2 joystick ports
- 4 buttons
- Micro SD slot
- GPIO 60 pins

### Front

![MC2P Front](MC2P-front.jpg)

### Back

![MC2P Back](MC2P-back.jpg)

### Joysticks

The DB9 joystick ports are configured to accept 6-button Mega Drive/Sega Genesis joysticks.

It is also possible to connect 3-button MD/SG and Atari 1-button joysticks.

*WARNING*: Do not connect MSX's joysticks because it will damage Multicore 2 Plus !!!

### Cassette Interface

The black P2 connector, between the green P2 connector and PS/2 keyboard connector, is the cassette interface of the Multicore 2 Plus.

![alt text](cassette_interface.png)

### Functions

#### Buttons

On Multicore 2 Plus' body you will find 4 colored buttons that may vary depending on the chosen Core. But, normally, their function is as follows:

- Yellow*: Reset
- Red: Insert credit (Arcade)
- Green: 1P Start (Arcade)
- Blue: 2P Start (Arcade)

\* At the Core's loading screen, pressing the yellow button will start the test mode.

#### Joystick

In the original Megadrive/Genesis' 6 button joystick, there is an button MODE, which is available to trigger some functions.

The joystick's functions are controlled by the loaded Core but, normally, will trigger those functions below on almost all:

- MODE + START: Open OSD
- MODE + B: Insert Credit (Arcade)
- MODE + X: Start 1P (Arcade)
- MODE + Y: Start 2P (Arcade)

Read the Core's documentation for more information about MODE's functions.

#### Keyboard

- F12: Opens the Core options menu
- ScrollLock: Toggle video output state (15khz)

### SD Card configuration

#### Auto loading

To auto load a Core, rename to `core.mcp` on SD card's root directory.
