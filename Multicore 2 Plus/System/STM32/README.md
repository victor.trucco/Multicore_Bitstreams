# Multicore 2 Plus



##### Change log


- 003 - 26/08/2021
        
        **STM versão 2.00

            1) Suporte a ExFAT
            Com os cartões SD ficando com capacidades cada vez maiores e mais baratos, adicionamos o suporte a ExFAT, então não é mais necessário formatar o seu cartão gigante em FAT32 com apenas 4Gb

            2) Suporte direto a arquivos ARC
            São arquivos gerados a partir do MRA tool para montar as roms, dipswitchs e arquivos de configuração, principalmente para hardwares de arcade.
            
            3) Suporte a imagens de disco (leitura e gravação)
            Para cores que usam formatações específicos, agora ficou fácil. Basta apenas um arquivo IMG da imagem do disco para ser "montada" pelo menu do core. Inclusive isso somente é necessário a escolha da primeira imagem, porque da próxima vez que o core for usado, ela será montada automaticamente. O uso de imagens também facilitará o agrupamento dos cores em um único cartão "gigante", independente do S.O. da máquina. Por exemplo não precisamos ter um cartão em FAT16 para o SM-X, apenas a imagem de disco de qualquer tamanho dentro de um cartão (e este pode estar com todos os outros cores). Também será útil no Color Computer (CoCo) que usa um sistema próprio de arquivos. Neste caso bastaria ter aquele mesmo IMG usado no cartão CF da MiniIDE. 
            
            4) Savegames
            Em alguns cores temos opções de salvar o jogo, o equivalente aos cartuchos que tem bateria interna. O procedimento é completamente transparente e não requer qualquer configuração, basta chegar no "savepoint" do jogo em questão.
            
            5) Velocidade de carregamento de ROMs
            Com algumas melhorias, agora cerca de 25% mais rápidas que a versão do firmware 1.10. Será particularmente notado em carregamentos longos, como alguns arcades.
            
            6) Sem limitação de quantidade de arquivos por pasta
            Finalmente não temos mais o limite de 128 arquivos por pasta. Agora é a limitação do próprio ExFAT (na teoria alguns milhares de arquivos e alguns milhares de pastas)
            
            7) Possibilidade de escolha da pasta padrão, para cada core.
            Você tem seus jogos em pastas separadas do core? Sem problema, basta informar onde e o core abre lá sempre que vc ligá-lo.
            
            8) Navegação correta para a pasta anterior
            Resolvido nesta versão também o inconveniente do "<..>" voltar para a raiz do SD e não para a pasta anterior como é mais intuitivo.
            
            9) Correção dos ".ini perdidos" dentro do cartão.
            Agora os .ini estão sempre juntos com o core. Sem arquivos duplicados em outras pastas.
            
            10) Ordenação otimizada de arquivos
            Com a quantidade MUITO maior de arquivos na pasta, foi necessário melhorar a ordenação e dado o humilde processamento do STM, o resultado é espantoso.
            
            11) Remoção de arquivos e pastas do sistema do menu
            Para quem usa Mac ou Windows, nada mais de arquivos "estranhos" e pastas criadas do nada. No menu veremos apenas os arquivos que nos interessam, referentes aos cores.
            
            12) Atualização de todas as bibliotecas e da IDE
            Praticamente todas as bibliotecas do STM32-arduino foram atualizadas, assim como a IDE de compilação. Isso proporcionou vários bug-fixes das bibliotecas oficiais, assim como uma redução significativa no tamanho final do binário para a programação do microcontrolador. (Tamanho do código caiu de 97% de ocupação pra 75%)
            
            13) Demais acertos e perfumaria
            São aquelas coisas que vamos fazendo e mudando durante a programação, mas nunca lembramos de documentar.

- 002 - 31/07/2020
        
        **STM versão 1.10c**

        - Carga otimizada de cores e ROMs (o tempo de carga diminui para 1/3 do tempo anterior)
        - Opção "Load Other Core" para trocar o core sem precisar desligar o equipamento.
        
- 001 - 01/10/2020

        **CORE versão 1.07**
        
        - Utilização do "common controls"
          (melhor gerenciamente do joystick de 6 botões no menu)

- 
