# Master System

Interface via Joystick :

- Direções: direcional do joystick
- 1 e 2: botões B e C
- Pause (SMS) ou Start (GG): botão Start

Quando um controle de 6 botões estiver conectado:

- OSD: MODE + START

2023/01/17 - Melhorias com o código mais recente / Fix pra detectar ROM GG / Fix botões 1 e 2 invertidos
2022/01/13 - Suporte a cartuchos externos com Expansion #01
2020/12/15 - Unificação da interface de Joystick de 6 botões, agora MODE + OSD chama o menu OSD.
2020/12/10 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE irá chamar o menu OSD. Correção para que o controle 8bitdo M30 2.4GHz funcione nesse core.