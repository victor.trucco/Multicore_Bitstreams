# Vectrex

Videogame Vectrex para o Multicore 2.

Os jogos utilizam de 1 a 4 botões, então é recomendado o uso do joystick da Sega, padrão 6 botões.

Utilize a tecla F12 para usar o menu e carregar os jogos.

O botão 4 faz o soft-reset do core.

MODE + START entra ou sai do OSD.

Copiar o arquivo "Vectrex.MC2" para a raiz do cartão SD.

##### Change log

- 004 : 26/12/2020 - Cima e baixo do controle 1 estava trocado com o controle 2 e vice versa, arrumado
- 003 : 15/12/2020 - Unificação da interface de Joystick de 6 botões, agora MODE + OSD chama o menu OSD.
- 003 : 10/12/2020 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE irá chamar o menu OSD.
- 002 : 03/11/2020 - inclusão de menu de opções
- 001 : 12/04/2018 - versão inicial