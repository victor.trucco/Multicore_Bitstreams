# Colecovision

Interface via Joystick :

- Direções: direcional do joystick
- Botões 1 e 2: botões B e C ou 1 e 2

Quando um controle de 6 botões estiver conectado:

- OSD: MODE + START
- 1, 2, 3, 4, 5, 6: MODE + (X, Y, Z, A, B, C)
- 7, 8, 9, ESTRELA, 0, #: START + (X, Y, Z, A, B, C)

Importante: No 8bitdo M30, manter o MODE pressionado por 3 segundos ou mais faz o controle funcionar em modo 3 botões (se fizer novamente, volta para 6 botões), então não mantenha MODE pressionado por muito tempo com esse controle.

2022/01/13 - Suporte a cartuchos externos com Expansion #01
2020/12/14 - A interface de joystick foi levada junto ao Coleco Keyboard, permitindo ativar o OSD pelo MODE + START bem como fazer as teclas 1, 2, 3, 4, 5 e 6 por MODE + (X, Y, Z, A, B e C); as teclas 7, 8, 9, ESTRELA, 0 e # são START + (X, Y, Z, A, B e C) e estrela, 0 e # são START + (X, Y, Z, A, B e C). Mudei o Right Action Button para C e o Left Action Button para B, jogar Tutankham é muito melhor. Agora é compatível com o 8bitdo M30.