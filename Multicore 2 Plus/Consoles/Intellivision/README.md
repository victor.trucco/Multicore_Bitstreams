# Intellivision

Interface via Joystick :

- Direções: direcional do joystick
- Baixo Esquerda, Baixo Direita e Superior (os dois são o mesmo botão): botões A, B e C

Quando um controle de 6 botões estiver conectado:

- OSD: MODE + START
- 1, 2, 3, 4, 5, 6: MODE + (X, Y, Z, A, B, C)
- 7, 8, 9, CLEAR, 0, ENTER: START + (X, Y, Z, A, B, C)


2020/12/15 - Uniformização do comportamento dos Joysticks de 6 botões, veja acima como usar o Keypad do controle Intellivision em um Joystick de 6 botões
2020/12/11 - Agora no joystick START é enter e X Y e Z são respectivamente 1, 2 e 3
2020/12/10 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE irá chamar o menu OSD.