Atari 5200

Após o carregamento do core é normal a tela do Atari ficar travado, por falta de um jogo. 
Carregue uma ROM utilizando o Menu OSD (tecla F12). 
Os chaveamentos dos jogos são identificados de acordo com o tamanho do arquivo, 
porém existem jogos de 16kb que utilizam 1 ou 2 cis de memória no cartucho. 
Selecione a opção do menu conforme o tipo de jogo carregado (somente é necessário para jogos de 16kb).

Alguns jogos fazem uso do gamepad do controle do Atari 5200, representado pela imagem abaixo

Start Pause Reset
  1     2     3
  4     5     6
  7     8     9
  *     0     #

As correspondências no teclado ficam assim

  F1    F2    F3
  1     2     3
  Q     W     E
  A     S     D
  Z     X     C

Segundo jogador fica mapeado:

  F4    F5    F6
  4     5     6
  R     T     Y
  F     G     H
  V     B     N

Joystick de Mega Drive:

As direções do joystick fazem a vez do joystick analógico no limite (pra cima, limite superior, etc)
Botões B e C são os dois botões de tiro do controle

Se tiver um controle de 6 botões:

X - Start
Y - Pause
Z - Reset

MODE + START - Chama o OSD ou desabilita o OSD

MODE + (X, Y, Z, A, B, C) - Keypad 1, 2, 3, 4, 5, 6
START + (X, Y, Z, A, B, C) - Keypad 7, 8, 9, ESTRELA, 0, #