# Genesis

Interface via Joystick :

- Direções: direcional do joystick
- Botões: os mesmos botões do controle são mapeados para o Genesis

Quando um controle de 6 botões estiver conectado:

- OSD: MODE + START

2024/01/19 - Pode habilitar SVP para jogar Virtua Racing, deixe desabilitado para outros jogos
2020/01/11 - Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
2020/12/15 - Uniformização da interface de Joystick 6 botões, OSD agora é MODE + START.
2020/12/10 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE irá chamar o menu OSD. Correção para que o controle 8bitdo M30 2.4GHz funcione nesse core.