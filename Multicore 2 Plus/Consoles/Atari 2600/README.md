# Atari 2600

Botões no Multicore 2+:

Botão Verde - Game Start ou se o OSD estiver ativo, para cima
Botão Azul - Game Select ou se o OSD estiver ativo, enter
Botão Vermelho - Se o OSD estiver ativo, para baixo
Botão Amarelo - Pressionar normal, invoca ou sai do OSD, pressionar por mais de 2s, reset

Controles com joystick de 6 botões:

- Direcional: as direções do controle
- Botões A, B ou START: botão do controle
- Botão C: ligado como segundo botão do controle, algum jogo suporta?
- MODE + START: entra ou sai do OSD
- MODE + X: chave RESET do console (inicia a maior parte dos jogos)
- MODE + Y: chave SELECT do console

DICA PARA QUEM TEM O 8BitDo M30:

Ficar com MODE pressionado por mais de 3 segundos transforma ele em um controle de 3 botões. Portanto, cuidado para não ficar com o MODE pressionado por muito tempo. Para voltar a funcionar como controle de 6 botões, fique com MODE apertado até a luz azul no receptor começar a piscar, indicando a mudança de modo.

2022/01/27 - Suporte a controles USB (necessita Host USB)
2022/01/13 - Suporte a cartuchos externos com Expansion #01
2021/02/01 - Nova interface dos botões frontais, permitindo usar o OSD sem teclado ou joystick
2021/01/28 - Adicionado os controles Start, Select e OSD aos botões 1,2,3 do Multicore 2+, respectivamente
2020/12/18 - Fix para uso de controles com 1 botão, também o fix não faz mais o segundo botão (C, só com controle de Mega) acionar o primeiro
2020/12/15 - Uniformizando a interface do joystick de 6 botões. MODE + START chama o OSD, MODE + X é igual 1 do teclado e MODE + Y é igual ao 2 do teclado.
2020/12/10 - Mudanças na interface de Joystick, em qualquer controle de Mega Drive o START funciona como a chave RESET (inicia maioria dos jogos) e no controle de 6 botões Y+Z funciona como a chave SELECT e o botão MODE irá chamar o menu OSD.
