# Game Boy Color

Interface via Joystick :

- Direções: direcional do joystick
- B e A: botões B e C
- Select: botão A
- Start: botão Start

Quando um controle de 6 botões estiver conectado:

- OSD: MODE + START

2020/12/15 - Unificação da interface de Joystick de 6 botões, agora MODE + OSD chama o menu OSD.
2020/12/10 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE irá chamar o menu OSD. Correção para que o controle 8bitdo M30 2.4GHz funcione nesse core.