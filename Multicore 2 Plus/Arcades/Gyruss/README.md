# Gyruss

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão B - Botão 1
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores

#Changelog

003 - 26/01/2021 - Adicionado um Double Frame Buffer com VSYNC, evitando o tearing ao custo da repetição ocasional de frames. Pode ser desligado. Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
002 - 26/12/2020 - Correção no clock do teclado, correção no contador do framebuffer.
001 - 17/12/2020 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE junto com o botão START irá chamar o menu OSD. O botão MODE junto com o botão X é o 1P Start, junto com Y é o 2P Start e junto com o B coloca créditos. Correção para que o controle 8bitdo M30 2.4GHz funcione nesse core.