# Galaga Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão B - Botão 1
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores

Botões frontais:

Botão Verde - Iniciar 1 jogador ou se o OSD estiver ativo, para cima
Botão Azul - Iniciar 2 jogadores ou se o OSD estiver ativo, enter
Botão Vermelho - Crédito ou se o OSD estiver ativo, para baixo
Botão Amarelo - Pressionar normal, invoca ou sai do OSD, pressionar por mais de 2s, reset

#Changelog

005 - 20/02/2021 - Adicionado Xevious e Super Xevious
004 - 01/02/2021 - Nova interface dos botões frontais, permitindo usar o OSD sem teclado ou joystick
003 - 26/01/2021 - Adicionado um Double Frame Buffer com VSYNC, evitando o tearing ao custo da repetição ocasional de frames. Pode ser desligado. Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
002 - 01/01/2021 - Adicionado o suporte a dar crédito e iniciar o jogo com o joystick de 6 botões
001 - 26/12/2020 - Primeira versão com framebuffer