# Namco Mappy Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão B e C - Botão 1 e 2
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores
MODE + B - Moeda
START + A - Pausa

Controle de Dois botões:

Botão A e B - Botão 1 e 2

15/01/2021 - Adicionado um Double Frame Buffer com VSYNC, evitando o tearing ao custo da repetição ocasional de frames. Pode ser desligado.
2021/01/11 - Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
2021/01/06 - Primeira versão para o MC2+