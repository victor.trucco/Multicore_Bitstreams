# CPS 1 Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão A, B , C, X, Y e Z - Botões 1, 2, 3, 4, 5 e 6
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores
MODE + B - Moeda
START + A - Pausa

2020/12/28 - Correção no comportamento em que sem joystick conectado, não era possível adicionar créditos ou jogar pelo teclado
2020/12/18 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE junto com o botão START irá chamar o menu OSD. O botão MODE junto com o botão X é o 1P Start, junto com Y é o 2P Start e junto com o B coloca créditos.