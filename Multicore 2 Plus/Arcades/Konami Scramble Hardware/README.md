# Konami Scramble Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão B, C, A e START - Botões 1, 2 , 3 e 4
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores

# Problemas conhecidos

Desde a primeira liberação para o MC2+ Calipso e Dark Planet estão com cores bugadas.
Desde a primeira liberação para o MC2+ Might Monkey só funciona se abrir o OSD, escolher para carregar rom alternativa e escolher o arquivo Might Monkey.dat

2021/01/18 - Adicionado um Double Frame Buffer com VSYNC, evitando o tearing ao custo da repetição ocasional de frames. Pode ser desligado. Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
2020/12/17 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE junto com o botão START irá chamar o menu OSD. O botão MODE junto com o botão X é o 1P Start, junto com Y é o 2P Start e junto com o B coloca créditos. Correção para que o controle 8bitdo M30 2.4GHz funcione nesse core.