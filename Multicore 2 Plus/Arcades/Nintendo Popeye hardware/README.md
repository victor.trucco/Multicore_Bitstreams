# Popeye / Sky Skipper

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botões B, C - Botões 1 e 2
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores

Controle de Dois botões:

Botão A e B - Botões 1 e 2

Nota: como Popeye tem um só botão, nesse jogo A, B, C e START são mapeados para ação

2021/01/12 - Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente. Popeye deve funcionar ok com joystick de 1 botão / Atari
2020/01/02 - Color fix
2020/12/30 - Primeira versão para MC2+