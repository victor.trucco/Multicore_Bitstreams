# Rygar

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão B e C - Botões 1 e 2
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores

2020/12/17 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE junto com o botão START irá chamar o menu OSD. O botão MODE junto com o botão X é o 1P Start, junto com Y é o 2P Start e junto com o B coloca créditos. Correção para que o controle 8bitdo M30 2.4GHz funcione nesse core.