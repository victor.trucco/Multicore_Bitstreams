# Capcom Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão A, B e C - Botões 3, 1, 2 (exceto GunSmoke, botões 1, 2 e 3)
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores
MODE + B - Moeda
START + A - Pausa

# Problemas conhecidos

Alguns dos jogos com framebuffer não exibem a primeira linha.

2021/01/23 - Scanlines, Video Direto, Framebuffer, rotação e duplo frame buffer (VSYNC) disponível em Black Tiger e Tiger Road também (todos os jogos agora)
2021/01/22 - OSD, Scanlines, Video Direto, Framebuffer, rotação e duplo frame buffer (VSYNC) disponíves em Ghosts'N Goblins, 1942, 1943, Commando, GunSmoke e Vulgus. Todos os jogos do Capcom HW podem ser renomeados agora desde que renomeie o dat com o mesmo nome.
2020/12/28 - Correção no comportamento em que sem joystick conectado, não era possível adicionar créditos ou jogar pelo teclado
2020/12/20 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE junto com o botão START irá chamar o menu OSD. O botão MODE junto com o botão X é o 1P Start, junto com Y é o 2P Start e junto com o B coloca créditos.