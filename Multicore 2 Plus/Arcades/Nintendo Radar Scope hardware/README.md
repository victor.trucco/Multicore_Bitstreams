# Nintendo Radarscope Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botões A, B , C e START - Ação
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores

Controle de Dois botões:

Botão A e B - Ação

Botões frontais:

Botão Verde - Iniciar 1 jogador ou se o OSD estiver ativo, para cima
Botão Azul - Iniciar 2 jogadores ou se o OSD estiver ativo, enter
Botão Vermelho - Crédito ou se o OSD estiver ativo, para baixo
Botão Amarelo - Pressionar normal, invoca ou sai do OSD, pressionar por mais de 2s, reset

# Changelog

- 005 - 01/02/2021 - Nova interface dos botões frontais, permitindo usar o OSD sem teclado ou joystick. Adicionado um Double Frame Buffer com VSYNC, evitando o tearing ao custo da repetição ocasional de frames. Pode ser desligado.
- 004 - 11/01/2021 - Fix pro botão do joystick de Atari ser reconhecido.
- 003 - 11/01/2021 - Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
- 002 - 04/01/2021 - Nova sintetização, com todos os cores integrados. Suporte aos Cores Donkey Kong, Donkey Kong Jr, Donkey Kong 3, Pest Place e Radarscope. Suporte a rotação de tela e sincronismo a 15Khz
- 001 - 15/12/2020 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE junto com o botão START irá chamar o menu OSD. O botão MODE junto com o botão X é o 1P Start, junto com Y é o 2P Start e junto com o B coloca créditos. Correção para que o controle 8bitdo M30 2.4GHz funcione nesse core.