# Bubble Bobble Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botões B e C - Botões 1 e 2
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores
MODE + B - Moeda
START + A - Pausa

Controle de Dois botões:

Botão A e B - Botões 1 e 2

15/01/2021 - Adicionado um Double Frame Buffer com VSYNC, evitando o tearing ao custo da repetição ocasional de frames. Pode ser desligado. Voltado ao teclado original do Jotego, a alteração anterior fez com que alguns teclados que funcionavam parassem de funcionar.
12/01/2021 - Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
2020/12/28 - Correção no comportamento em que sem joystick conectado, não era possível adicionar créditos ou jogar pelo teclado
2020/12/20 - Corrigido problema do Joystick 2 o botão 1 acionar tanto o botão 1 como o 2
2020/12/18 - Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE junto com o botão START irá chamar o menu OSD. O botão MODE junto com o botão X é o 1P Start, junto com Y é o 2P Start e junto com o B coloca créditos.