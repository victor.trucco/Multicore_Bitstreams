# Konami Green Beret Hardware

Compatível com joystick Sega de Seis Botões

Direcionais - Controla o personagem
Botão B e C - Botão 1 e 2
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores
MODE + B - Moeda

# Changelog

- 003 - 18/01/2021 - Adicionado um Double Frame Buffer com VSYNC, evitando o tearing ao custo da repetição ocasional de frames. Pode ser desligado. Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
- 002 - 01/01/2021 - Framebuffer para compatibilizar a saída a 60hz, modo 15khz e mudanças cosméticas no core.
- 001 - 29/12/2020 - Primeira versão para o MC2+