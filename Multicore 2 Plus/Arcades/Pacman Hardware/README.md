# Pac Man Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão B - Botão 1
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores
MODE + B - Moeda

Controle de Dois botões:

Botão A - Ação

#Changelog

005 - 11/01/2021 - Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
004 - 01/01/2021 - Adicionados Pacman e vários clones (ainda falta Woodpecker e Super Globber)
003 - 30/12/2020 - Framebuffer -> Agora pode rotacionar a tela
002 - 30/12/2020 - Color fix
001 - 29/12/2020 - Primeira versão, convertida do MC2 por Oduvaldo