# Item M52 Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão B e C - Botões 1 e 2
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores

#Changelog

001 - 28/01/2021 - Teclado com novo timing e melhor compatibilidade para Moon Patrol e Traverse USA, Traverse USA agora tem duplo framebuffer e VSYNC habilitando o duplo framebuffer no OSD.