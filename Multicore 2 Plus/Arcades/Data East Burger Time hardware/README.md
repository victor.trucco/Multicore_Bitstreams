# Data East Burger Time Hardware

Controle de Seis Botões Mega Drive:

Direcionais - Controla o personagem
Botão B - Botão 1
MODE + START - Inicia o OSD ou sai do OSD
MODE + X - Inicia o jogo, 1 jogador
MODE + Y - Inicia o jogo, 2 jogadores
MODE + B - Moeda

#Changelog

003 - 18/01/2021 - Adicionado um Double Frame Buffer com VSYNC, evitando o tearing ao custo da repetição ocasional de frames. Pode ser desligado. Novo timing no teclado, torna compatível com alguns teclados que não funcionavam anteriormente.
002 - 06/01/2021 - Correção do funcionamento errático do teclado principalmente com o F12
001 - 04/01/2021 - Primeira versão, convertida do MC2 por Oduvaldo com framebuffer e rotação