PACMAN hardware

CHANGELOG

002 - 11/05/2020 - Som no HDMI 
                 - Corrigido o "flip bit" no Dream Shopper e Van Van Car
                 - Resolução dinâmica, de acordo com a rotação (640x480 ou 800x600)
                 - Rotação da imagem por hardware a 90o, 180o e 270o.

001 - 03/05/2020 - Versão inicial