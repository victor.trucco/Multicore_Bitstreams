# Vectrex

Videogame Vectrex para o Multicore 2.

Os jogos utilizam de 1 a 4 botões, então é recomendado o uso do joystick da Sega, padrão 6 botões.

Utilize a tecla F12 para usar o menu e carregar os jogos.

O botão 4 faz o soft-reset do core.


Copiar o arquivo "Vectrex.MC2" para a raiz do cartão SD.

##### Change log

- 001 : 03/11/2020 - versão inicial