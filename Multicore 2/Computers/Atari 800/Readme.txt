ATARI 8 bits

Atenção para a estrutura das pastas no cartao SD. Não pode ser mudada!

Atari800 (raiz do cartão)
       |
       ----- ROMS (contém as ROMs do sistema, não mexer)
       |
       ----- USER (onde ficam os arquivos dos jogos)

Teclas:
F5  - Help
F6  - Start
F7  - Select
F8  - Option
F9  - Reset
F10 - Cold start (limpa os primeiros 64KB de RAM e reseta) 
F11 - Seleciona a imagem do drive 1 e então dá um cold start
F12 - Menu

    Turbo - velocidade do sistema
        1x (default): mais compativel - cerca de 1.7MHz
        2x: 3.4MHz 
        4x: 6.8MHz 
        8x: 13Mhz
        16x: 27MHZ

    RAM
        64KB:  (exemplo 65XE)
        128KB: (exemplo 130XE), 64KB ext ram
        320KB(Compy shop)(default): 256KB ext ram
        320KB(Rambo): 256KB ext ram
        576KB(Compy shop): 512KB ext ram 
        576KB(Rambo): 512KB ext ram
        1088KB: 1024KB ext ram
        4160KB: incompativel com muitos programas

    ROM
        Seleção de um system OS ROM diferente- pode ser 16KB ou 10KB

    Drive
        Left: Remove o disco
        Right: Seleção de arquivo 
        Enter: Insere o disco

    Cartridge
        Left: Remove o cartucho
        Right: Seleção de arquivo
        Fire: Insere o cartucho

System ROM:
    carregado de /ATARI800/ROM/atarixl.rom

Basic:
    carregado de /ATARI800/ROM/ataribas.rom

Disk images:
    Pasta default: /ATARI800/USER
    Tipos de arquivos: 
        .ATR - Atari disk image com header. single/medium/double density.
        .XFD - Atari disk image sem header. 
        .XEX - Atari executable. Um bootloader é carregado antes, mas não é 100% compativel. 

Cartuchos:
    Default dir: /ATARI800/USER
    Tipos de arquivos: 
        .CAR - Atari 800 cartridge com header.
        Os qrquivos .BIN precisam ser convertidos com alguma ferramenta para poder serem usados (exemplo Atari800WinPlus).

ATENÇÃO!!!
    Muitos programas e jogos precisam do BASIC desbilitado, como na máquina real. Mantenha "Option" (tecla F8) pressionado durante o reset.
