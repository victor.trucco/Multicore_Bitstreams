# CP/M

Colocar o arquivo "CPM3.MC2" na raiz do cartão SD.

Necessita de um cartão SD em formato próprio. Descompacte o zip e utilize um programa como o Etcher ou Win32DiskImage para gravar a imagem no cartão. Após colocar o cartão no slot, acionar o reset (botão 3 + botão 4) para que o SD sejá "montado" pelo sistema.

Este core é baseado no projeto de autoria de Grant Searle. http://searle.hostei.com/grant/Multicomp/cpm/fpgaCPM.html

##### Uso

Reset: Botão 3 + botão 4

##### Change log

- 003 : 14/08/2018 - Incluida a imagem para o cartão SD
- 002 : 20/02/2018 - Suporte ao Multicore 2
- 001 : 24/11/2016 - versão inicial
