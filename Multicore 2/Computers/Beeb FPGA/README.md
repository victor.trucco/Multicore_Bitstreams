# BBC Micro e BBC 128

Microcomputadores BBC para o Multicore 2.

O botão 1 faz o soft-reset do core. Mantendo o botão 2 pressionado por mais de 1 segundo comuta entre as versões do BBC Micro e do BBC 128.

Para o funcionamento do interface SD é necessário um arquivo contendo as imagens dos softwares. Esse arquivo deverá estar na raiz do cartão com o nome de "BEEB.MMB". Não posso fornecer o arquivo por conter material com copyright, mas é bem simples de encontrá-lo em mecanismos de buscas e fóruns especializados no BBC Micro.

O arquivo "b128_rom.dat" assim como "bbc_micro.mc2" devem estar presentes na raiz do cartão SD.


##### Change log

- 001 : 09/05/2018 - versão inicial