Quick port from EDSAC FPGA core (originaly for MiSTer)

Loading a program
   General loading procedure:
   
   F12, load tape, select tape image
   F3, switch to panel display
   Press "I" on the keyboard to erase memory and load initial orders
   Press "R" to reset
   If HALT light is on, press "C" to start execution

Keyboard shortcuts
   0-9 - enter number using the telephone dial
   F1 - CRT screen
   F2 - Teletype output
   F3 - Panel
   F12 - On Screen Menu
   I - Clear memory and write initial orders
   E - Erase teletype screen
   R - Reset
   H - Halt computer
   C - Continue execution (resume)


Check the author Github for more info
https://github.com/hrvach/EDSAC