# Apple II

- VGA
- Drive DiskII
- suporte preliminar a Mocking Board

Formato do SD: RAW, formato próprio.

Verifique a pasta "/Utils" para maiores informações sobre a imagem dos discos para o cartão SD

#### Uso

Colocar o arquivo "AppleII.MC2" na raiz de um cartão SD para o boot. Após iniciado, trocar o SD para o cartão em formato RAW.

##### Change log

- 003 : 09/07/2018 - versão inicial Multicore 2
- 002 : 19/12/2016 - melhorado as cores com scanlines
- 001 : 13/12/2016 - versão inicial