HT1080Z - Clone do TRS80 Model I

- Utilize o Botão 4 para RESET
- F5 seleção da cor dos caracteres
- F6 seleção da cor de fundo
- F7 seleção da cor da borda

Para carregar uma imagem CAS

no boot, READY? pressione ENTER, e ENTER novamente
- digite SYSTEM
- abra o OSD com F12 e selecionar o arquivo CAS
- digite a primeira letra do nome arquivo carregado. Exemplo, para o "Galaxy.cas", digite G no prompt e ENTER
- digite / e ENTER para executar 