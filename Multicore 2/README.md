# Multicore 2 Bitstreams

Bistreams pronto para uso no Multicore 2.

Consulte informações específicas do Core dentro de cada pasta.

# Microcomputers

<b>(soon)</b>

# Video games

<b>(soon)</b>

# Arcades

Arcades root folder  <b>(5 cores)</b>
   - Asteroids
   - BombJack
   - Centipede
   - Crazy Ballon
   - Tetris

Arcades/Atari BW Raster hardware <b>(6 cores)</b>
   - CanyonBomber
   - Dominos
   - Sprint2
   - SprintOne
   - SuperBreakout
   - Ultratank     

Arcades/Atari Discrete Logic <b>(1 core)</b> 
   - ComputerSpace
               
Arcades/Atari Vector <b>(1 core)</b> 
   - LunarLander
               
Arcades/Bagman hardware <b>(5 cores)</b> 
   - Bagman
   - Botanic
   - Pickin
   - Squash
   - SuperBagman
               
Arcades/Berzerk hardware <b>(3 cores)</b>
   - Berzerk
   - Frenzy
   - MoonWar
               
Arcades/Capcom hardware <b>(8 cores)</b>
   - 1942
   - 1943
   - BlackTiger
   - Commando
   - GNG
   - GunSmoke
   - TigerRoad
   - Vulgus
   
Arcades/Capcom CPS1 hardware <b>(30 cores)</b>
   - 1941 Counter Attack
   - Captain 
   - Carrier Air Wing
   - Dynast Wars
   - Final Fight
   - Forgotten Worlds
   - Ghouls and Ghosts
   - Knights of the Round
   - Magic Sword
   - Mega Man
   - Mega Twins
   - Mercs
   - Nemo
   - Pang 3
   - Pnickies
   - Pokoyan Ballon
   - Quiz and Dragons
   - Street Fighter Alpha Warriors Dreams
   - Street Fighter Zero
   - Street Fighter II - The World Warrior
   - Street Fighter II - Champion Edition
   - Street Fighter II - Hyper Fighting
   - Street Fighter II - Turbo Hyper Fighting
   - Strider
   - The King of Dragons
   - Three Wonders
   - U.N. Squadron
   - Varth
   - Warriors of Fate
   - Willow
   
Arcades/Capcom SonSon hardware <b>(1 core)</b>
   - SonSon
   
Arcades/Crazy Climber hardware <b>(4 cores)</b>
   - Crazy Climber
   - Crazy Kong
   - RiverPatrol
   - SilverLand
                
Arcades/Data East Burger Time hardware <b>(2 cores)</b>
   - BurgerTime
   - BurninRubber
               
Arcades/Double Dragon arcade <b>(2 cores)</b> 
   - DoubleDragon
   - DoubleDragon2
               
Arcades/Galaga hardware <b>(3 cores)</b> 
   - DigDug
   - Galaga
   - Xevious
               
Arcades/Galaxian hardware <b>(18 cores)</b> 
   - Azurian Attack
   - Blackhole
   - Catacomb
   - Chewing Gum
   - Devil Fish
   - Galaxian
   - King Ballon
   - Lucky Today
   - Moon Cresta
   - Mr. Dos Nightmare
   - Omega
   - Orbitron
   - Pisces
   - Triple Draw
   - Uniwars
   - Victory
   - War of the Bugs
   - ZigZag
          
Arcades/Irem M52 Hardware <b>(2 cores)</b>
   - Moon Patrol
   - TraverseUSA  
   
Arcades/Irem M57 Hardware <b>(1 core)</b>
   - TropicalAngel
                  
Arcades/Irem M62 Hardware <b>(11 cores)</b> 
   - Battle Road
   - Horizon
   - Kid Niki
   - Kung Fu Master
   - Lode Runner 2
   - Lode Runner 3
   - Lode Runner 4
   - Lode Runner
   - Spelunker 2
   - Spelunker
   - Youjyudn

Arcades/Konami Classic Hardware <b>(3 cores)</b>     
   - Pooyan
   - Power Surge
   - Time Pilot
   
Arcades/Konami Green Beret Hardware <b>(2 cores)</b>     
   - GreenBeret
   - MrGoemon
              
Arcades/Konami Scramble Hardware <b>(20 cores)</b>  
   - Amidar
   - Ant Eater
   - Armored Car
   - Battle of Atlantis
   - Calipso
   - Dark Planet
   - Frogger
   - Lost Tomb
   - Mars
   - Mighty Monkey
   - Minefield
   - Moon War
   - Rescue
   - Scramble
   - Speed Coin
   - Strategy X
   - Super Cobra
   - Tazzmania
   - TheEnd
   - Turtles
              
Arcades/Ladybug hardware <b>(4 cores)</b> 
   - CosmicAvenger
   - Dorodon
   - LadyBug
   - Snapjack
              
Arcades/Midway MCR 1 <b>(4 cores)</b> 
   - Draw Poker
   - Kick Man
   - Kick
   - Solar Fox
               
Arcades/Midway MCR 2 <b>(7 cores)</b>  
   - Domino Man
   - Journey
   - Kozmik Kroozr
   - Satans Hollow
   - Tron
   - Two Tigers
   - Wacko
                  
Arcades/Midway MCR 3 <b>(4 cores)</b>   
   - Demolition Derby
   - Discs Of Tron
   - Tapper
   - Timber
                 
Arcades/Midway MCR 3 Monoboard <b>(4 cores)</b>   
   - MaxRPM
   - PowerDrive
   - Rampage
   - Sarge
                  
Arcades/Midway MCR Scroll <b>(3 cores)</b>   
   - CraterRaider
   - SpyHunter
   - TurboTag
                  
Arcades/Midway-Taito 8080 Hardware <b>(7 cores)</b>      
   - Lunar Rescue
   - Ozma Wars
   - Space Invaders 2
   - Space Invaders
   - Space Laser
   - Super Earth Invasion
   - Vortex
               
Arcades/Namco Mappy Hardware <b>(4 cores)</b>     
   - DigDug2
   - Mappy
   - Motos
   - TowerofDruaga

Arcades/Namco Rally X hardware <b>(2 cores)</b>     
   - Rally X
   - New Rally X
               
Arcades/Nintendo Popeye hardware <b>(2 cores)</b>    
   - Popeye
   - Sky Skipper
               
Arcades/Nintendo Radar Scope hardware  <b>(2 cores)</b>    
   - DonkeyKong
   - DonkeyKongJr
              
Arcades/Nova2001_Hardware  <b>(1 core)</b>     
   - Ninjakun
                
Arcades/Pacman hardware <b>(24 cores)</b>  

   - Alibaba
   - Beastie Feastie
   - Birdiy
   - CrushRoller
   - DreamShopper
   - Eeekk
   - Eggor
   - Eyes
   - Gorkans
   - Jump Shot
   - LizardWizard
   - ManiacMiner
   - MrTNT
   - MSPacman
   - Pacman
   - PacmanClub
   - PacmanPlus
   - Pengo
   - Ponpoko
   - Puckman
   - SuperGlob
   - The Glob
   - VanVanCar
   - Woodpecker
                
Arcades/Phoenix hardware <b>(3 cores)</b>   
   - Capitol
   - Phoenix
   - Pleiads
               
Arcades/Sega Zaxxon Hardware <b>(2 cores)</b>
   - Zaxxon
   - Supper Zaxxon
               
Arcades/Williams 6809 rev.1 Defender Hardware <b>(4 cores)</b>   
   - Colony7
   - Defender
   - Jin
   - Mayday
                 
Arcades/Williams 6809 rev.1 Robotron Hardware <b>(6 cores)</b>      
   - Bubbles
   - Joust
   - Robotron
   - Sinistar
   - Splat
   - Stargate
              

<b>TOTAL ARCADES: 211</b>
