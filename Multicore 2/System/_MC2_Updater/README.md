# Multicore 2 atualizador

O Multicore 2 funciona com base em dois "firmwares" diferentes. 

O primeiro funciona no microcontrolador STM32 e controla o carregamento dos cores, enquanto o segundo é diretamente na memória flash do FPGA, montando o menu inicial e fazendo a leitura do teclado e joystick.

Não acredito que será um procedimento comum atualizar quaisquer desses firmwares uma vez que o projeto já estiver em pleno funcionamento, mas como nunca sabemos o dia de amanha, um método de atualização se mostrou necessário devido ao fato do MC2 não utilizar cabos conectados ao PC. No nosso caso utilizaremos o próprio cartão SD para fazer a transferência e atualização dos cores.

A versão atual dos firmwares são informados na tela inicial do MC2, logo depois de ligá-lo.
[![ScreenShot](Menu1.JPG)]
[![ScreenShot](Menu2.JPG)]

O procedimento de atualização é muito simples, porém exige pequenos cuidados.

#### Atualizando o firmware do STM
Arquivo
https://gitlab.com/victor.trucco/Multicore_Bitstreams/-/blob/master/Multicore%202/_MC2_Updater/STM_update_V200.zip

Descompacte o conteúdo do zip em um cartão recém-formatado (não importa o formato, FAT e FAT32 funcionam). O cartão não deve conter outros arquivos além dos que estão no zip. Não misture arquivos do CORE e do STM. Faça as atualizações separadamente, uma por vez, formatando novamente o cartão entre as atualizações.

Quando o atualizador for carregado, é possivel notar a borda na cor azul claro e um pedido para trocar o jumper do microcontrolador.
[![ScreenShot](Step_1_1.JPG)]

Infelizmente para ter acesso a este jumper é necessário remover a tampa de acrilico do MC2.
[![ScreenShot](stm32.jpg)]

A posição correta para a atualização é mover o jumper da esquerda para a posição 1.
[![ScreenShot](stm32_02.jpg)]

Após pressionar ENTER e confirmar, a atualização se inicia.
[![ScreenShot](Step_1_2.JPG)]

**ATENÇÃO: não desligue o MC2 durante a atualização**. 

Ao final do processo, vemos a confirmação (borda verde) e o pedido para voltar com o jumper para a posição inicial.
[![ScreenShot](Step_1_3.JPG)]

**ATENÇÃO: se houver alguma mensagem de erro, NÃO DESLIGUE**. Basta pressionar o botão 1 do MC2 que todo o processo se reinicia. 
[![ScreenShot](Step_1_error.JPG)]

#### Atualizando o firmware do CORE
Arquivo
https://gitlab.com/victor.trucco/Multicore_Bitstreams/-/blob/master/Multicore%202/_MC2_Updater/CORE_update_V106.zip

Descompacte o conteúdo do zip em um cartão recém-formatado (não importa o formato, FAT e FAT32 funcionam). O cartão não deve conter outros arquivos além dos que estão no zip. Não misture arquivos do CORE e do STM. Faça as atualizações separadamente, uma por vez, formatando novamente o cartão entre as atualizações.

Quando o atualizador for carregado, é possivel notar a borda na cor azul escura e um pedido para confirmar a atualização
[![ScreenShot](Step_2_1.JPG)]

Logo após a atualização se inicia.
[![ScreenShot](Step_2_2.JPG)]

**ATENÇÃO: não desligue o MC2 durante a atualização**. 

Ao final do processo, vemos a confirmação (borda verde) e o pedido para desligar a fonte do MC2.
[![ScreenShot](Step_2_3.JPG)]



##### Change log


- 011 - 26/08/2021
        
        **STM versão 2.00

            1) Suporte a ExFAT
            Com os cartões SD ficando com capacidades cada vez maiores e mais baratos, adicionamos o suporte a ExFAT, então não é mais necessário formatar o seu cartão gigante em FAT32 com apenas 4Gb

            2) Suporte direto a arquivos ARC
            São arquivos gerados a partir do MRA tool para montar as roms, dipswitchs e arquivos de configuração, principalmente para hardwares de arcade.
            
            3) Suporte a imagens de disco (leitura e gravação)
            Para cores que usam formatações específicos, agora ficou fácil. Basta apenas um arquivo IMG da imagem do disco para ser "montada" pelo menu do core. Inclusive isso somente é necessário a escolha da primeira imagem, porque da próxima vez que o core for usado, ela será montada automaticamente. O uso de imagens também facilitará o agrupamento dos cores em um único cartão "gigante", independente do S.O. da máquina. Por exemplo não precisamos ter um cartão em FAT16 para o SM-X, apenas a imagem de disco de qualquer tamanho dentro de um cartão (e este pode estar com todos os outros cores). Também será útil no Color Computer (CoCo) que usa um sistema próprio de arquivos. Neste caso bastaria ter aquele mesmo IMG usado no cartão CF da MiniIDE. 
            
            4) Savegames
            Em alguns cores temos opções de salvar o jogo, o equivalente aos cartuchos que tem bateria interna. O procedimento é completamente transparente e não requer qualquer configuração, basta chegar no "savepoint" do jogo em questão.
            
            5) Velocidade de carregamento de ROMs
            Com algumas melhorias, agora cerca de 25% mais rápidas que a versão do firmware 1.10. Será particularmente notado em carregamentos longos, como alguns arcades.
            
            6) Sem limitação de quantidade de arquivos por pasta
            Finalmente não temos mais o limite de 128 arquivos por pasta. Agora é a limitação do próprio ExFAT (na teoria alguns milhares de arquivos e alguns milhares de pastas)
            
            7) Possibilidade de escolha da pasta padrão, para cada core.
            Você tem seus jogos em pastas separadas do core? Sem problema, basta informar onde e o core abre lá sempre que vc ligá-lo.
            
            8) Navegação correta para a pasta anterior
            Resolvido nesta versão também o inconveniente do "<..>" voltar para a raiz do SD e não para a pasta anterior como é mais intuitivo.
            
            9) Correção dos ".ini perdidos" dentro do cartão.
            Agora os .ini estão sempre juntos com o core. Sem arquivos duplicados em outras pastas.
            
            10) Ordenação otimizada de arquivos
            Com a quantidade MUITO maior de arquivos na pasta, foi necessário melhorar a ordenação e dado o humilde processamento do STM, o resultado é espantoso.
            
            11) Remoção de arquivos e pastas do sistema do menu
            Para quem usa Mac ou Windows, nada mais de arquivos "estranhos" e pastas criadas do nada. No menu veremos apenas os arquivos que nos interessam, referentes aos cores.
            
            12) Atualização de todas as bibliotecas e da IDE
            Praticamente todas as bibliotecas do STM32-arduino foram atualizadas, assim como a IDE de compilação. Isso proporcionou vários bug-fixes das bibliotecas oficiais, assim como uma redução significativa no tamanho final do binário para a programação do microcontrolador. (Tamanho do código caiu de 97% de ocupação pra 75%)
            
            13) Demais acertos e perfumaria
            São aquelas coisas que vamos fazendo e mudando durante a programação, mas nunca lembramos de documentar.

- 010 - 31/07/2020
        
        **STM versão 1.10c**

        - Carga otimizada de cores e ROMs (o tempo de carga diminui para 1/3 do tempo anterior)
        - Opção "Load Other Core" para trocar o core sem precisar desligar o equipamento.
        
- 009 - 01/10/2020

        **CORE versão 1.06**
        
        - Utilização do "common controls"
          (melhor gerenciamente do joystick de 6 botões no menu)

- 008 - 17/03/2020
        
        **STM versão 1.08**

        - arquivos .INI para os cores
        
- 007 - 09/03/2020
        
        **STM versão 1.07**
        
        - Splash Screen 
        - Ordenação alfabética de pastas e arquivos
        - Possibilidade de Cores dentro de pastas
        - 128 arquivos por pasta
        - Novo gerenciamento de teclado
        - Navegação pelas letras no menu
        - Auto-repetição de teclas
        
        
        **CORE versão 1.05**
        
        - Gerenaciamento do teclado
        - Auto-teste (botão 4)
        
        
- 006 - 25/01/2020 - STM versão 1.06. CORE versão 1.04
- 005 - 02/12/2019 - STM versão 1.05. CORE versão 1.03
- 004 - 16/07/2019 - STM versão 1.04.
- 003 - 31/03/2019 - STM versão 1.03.
- 002 - 15/05/2018 - STM versão 1.02. CORE versão 1.01 - Obrigado ao Mauro Passarinho pela ajuda nos testes.
- 001 - 12/04/2018 - STM versão 1.01. CORE versão 1.01
