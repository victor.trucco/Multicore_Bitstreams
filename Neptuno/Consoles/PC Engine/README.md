# PC Engine

# Changelog

2021/04/12 

- Reforma completa do core, para o Multicore 2 Plus
- Várias acertos e melhorias no processador HuC6280, corrigindo alguns jogos
- Suporte ao Super Grafx, console sucessor do PC Engine
- Suporte ao controle de 6 botões, utilizado em alguns jogos
- Menu padrão, o mesmo utilizados nos outros cores

