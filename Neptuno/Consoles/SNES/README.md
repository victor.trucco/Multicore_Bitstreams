# SNES

# Changelog

2021/04/12 

- Timing perfeito no HDMI
- Seleção automática de tipos de mapeamento de memória
- Suporte ao formato SMC
- Padronização da interface usando o "common controls"	

2020/12/11 

- Mudanças na interface de Joystick, no controle de Mega Drive de 6 botões o botão MODE junto com o botão START irá chamar o menu OSD. 
- Correção para que o controle 8bitdo M30 2.4GHz.