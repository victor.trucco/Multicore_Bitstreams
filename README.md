# Multicore Bitstreams

Bistreams pronto para uso no Multicore, Multicore 2 e Multicore 2+. (Atenção, pastas separadas para as três versões)

Consulte informações específicas de cada Core dentro das pastas específicas.

Os bitstreams estão disponíveis nas versões ".SOF" para programação do Multicore em soft-mode onde a programação do FPGA é perdida quando a fonte de alimentação é desligada e ".JIC" para a programação da memória flash da placa. Uma vez programada a flash, a placa pode ser iniciada sem o cabo conectado, com o último bitstream .JIC programado. O uso de um .SOF não apaga o .JIC da placa, sendo ideal para experimentar outras plataformas sem precisa reprogramar a flash todas as vezes. 
Já o Multicore 2 usa arquivos com a extensão ".MC2", e pode ter arquivos ".INI" e ".DAT" associados, dependendo de cada core. Estes arquivos devem ser mantidos juntos em uma pasta ou mesmo gravado na raiz do cartão.

Para o Multicore 2+ usamos a extensão ".MCP", que também pode ter ".INI" e ".DAT" associados, basta mante-los juntos. ATENÇÃO: os arquivos ".MC2" e ".MCP" não são intercambiáveis!

Alguns Cores tem saída de vídeo em HDMI ou VGA. Para o caso do Multicore, note que é preciso programar a versão de sua preferência na placa, já que não é possível utilizar as duas saídas de vídeo ao mesmo tempo por limitações do hardware. Para o Multicore 2 a saída é simultânea nos dois conectores de video, caso o core tenha opção para HDMI.

Para gravação dos Cores no Multicore é necessário ter instalado a ferramenta de programação stand-alone da Altera. Para o Multicore 2 ou o Multicore 2+ esse procedimento não é necessário, basta copiar o arquivo ".mc2" ou ".mcp" para o cartão e o microcontrolador presente na placa enviará o bitstream para o FPGA.

Versões para Windows e Linux estão disponíveis no link
https://www.altera.com/downloads/software/prog-software/121.html

A versão completa do Quartus II também poderá ser usada, mas o arquivo para instalação é substancialmente maior.
http://dl.altera.com/13.1/?edition=web

O amigo Mauro Passarinho fez uma passo-a-passo em vídeo mostrando a instalação do driver do USB Blaster no Windows 10. É provavel que sirva também em outras versões do Windows, caso o sistema não identifique automaticamente o driver. (Clique na imagem abaixo para ver o video)


[![ScreenShot](http://network.bbtv.com/pt/wp-content/uploads/sites/2/2016/02/Postar-V%C3%ADdeo-no-Youtube.png)](https://www.youtube.com/watch?v=wUNlaITtjcU)

Sugestões de etiquetas para cartões SD. (Autor: Mauro Passarinho)
https://gitlab.com/victor.trucco/Multicore_Bitstreams/blob/master/Labels.rar